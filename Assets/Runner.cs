﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class Runner : CharacterAbility {


	CharacterRun characterRun;
	CharacterHorizontalMovement characterHorizontalMovement;
	TrackManager trackManager;
	GameObject AIFollower;
	bool enemyMoved = false;

	[SerializeField] float speed;

	// Use this for initialization
	void Start () {
		base.Initialization ();
		characterRun = GetComponent<CharacterRun> ();
		characterHorizontalMovement = GetComponent<CharacterHorizontalMovement> ();
		trackManager = FindObjectOfType<TrackManager> ();
	
		AIFollower = FindObjectOfType<AIFollow> ().gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		if (trackManager.hasBegin) {
			AutoRun ();
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.layer == 13) {
			Debug.Log ("GAME OVER");
		}
	}

	void AutoRun(){
		if (_controller.State.IsCollidingRight) {
			_movement.ChangeState(CharacterStates.MovementStates.Idle);	
//			if (!enemyMoved) {
//				Vector3 newAIPos = new Vector3 (transform.position.x-10, transform.position.y,transform.position.z);
//				AIFollower.transform.position = newAIPos;
//				enemyMoved = true;
//			}
			return;
		}
		_controller.SetHorizontalForce(speed*16/100);
		_movement.ChangeState(CharacterStates.MovementStates.Walking);	
//		enemyMoved = false;
	}

	void StopAutoRun(){
		Vector3 test = new Vector3(Time.deltaTime*0,0,0);
		transform.Translate (test, Space.Self);
		Debug.Log ("Stopped");


	}
}
