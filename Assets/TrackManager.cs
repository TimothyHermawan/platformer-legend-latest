﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class TrackManager : MonoBehaviour {

	[SerializeField] Track[] availablePlatform;
	[SerializeField] float minSpace;
	[SerializeField] float maxSpace;
	[SerializeField] GameObject parentObject;

	private GameObject AIFollower;
	private GameObject player;
	private GameObject playerCamera;
	private Transform firstSpawnPosition;
	private float firstXPos;
	private Track[] tracks;


	public bool hasBegin = false;

	void Start () {
		Track firstTrack = GetFirstTrack ();
		firstSpawnPosition = firstTrack.transform;
		firstXPos = firstSpawnPosition.position.x;
		player = GameObject.FindGameObjectWithTag ("Player");
		playerCamera = Camera.main.gameObject;
		AIFollower = GameObject.FindObjectOfType<AIFollow> ().gameObject;
	}

	public void SpawnPlatform(){
		Track lastTrack = GetLastTrack ();
		Vector3 offset = new Vector3 (Random.Range(minSpace,maxSpace), 0, 0);
		Track thing = availablePlatform [Random.Range (0, availablePlatform.Length)];
		float thingLong = Mathf.Abs (thing.endPoint.position.x - thing.startPoint.position.x);
		Vector3 thingPosition = new Vector3 (lastTrack.endPoint.position.x + thingLong/2, thing.transform.position.y, 0);
		Instantiate (thing.gameObject, thingPosition+offset, Quaternion.identity);

//		Debug.Log ("Last Track endPos = " + lastTrack.endPoint.position.x);

		Track firstTrack = GetFirstTrack ();
//		Debug.Log ("First Track = " + firstTrack.name);
		Destroy (firstTrack.gameObject);
	}

	Track GetLastTrack(){
		tracks = FindObjectsOfType<Track>();
		Track randomTrack = FindObjectOfType<Track> ();
		Track lastTrack = randomTrack;
		float distance = randomTrack.gameObject.transform.position.x;
		foreach (Track t in tracks) {
			if (t.transform.position.x > distance) {
				lastTrack = t;
			}
		}
		return lastTrack;
	}

	Track GetFirstTrack(){
		tracks = FindObjectsOfType<Track>();
		Track randomTrack = FindObjectOfType<Track> ();
		Track firstTrack = randomTrack;
		float distance = randomTrack.gameObject.transform.position.x;
		foreach (Track t in tracks) {
			if (t.transform.position.x < distance) {
				firstTrack = t;
			}
		}
		return firstTrack;
	}

	public void ReturnTrackToFirstPlace(){
		tracks = FindObjectsOfType<Track> ();
		Track firstTrack = GetFirstTrack ();
		float distanceToFirstSpawnPosition = Mathf.Abs(firstTrack.transform.position.x - firstXPos);
		foreach (Track t in tracks) {
			float newXPos = t.transform.position.x - distanceToFirstSpawnPosition;
			Vector3 newPos = new Vector3(newXPos, t.transform.position.y, t.transform.position.z);
			t.transform.position = newPos;
		}
		Vector3 newPlayerPos = new Vector3 (player.transform.position.x - distanceToFirstSpawnPosition, player.transform.position.y, player.transform.position.z);
		player.transform.position = newPlayerPos;

		Vector3 newCameraPos = new Vector3 (playerCamera.transform.position.x - distanceToFirstSpawnPosition, playerCamera.transform.position.y, playerCamera.transform.position.z);
		playerCamera.transform.position = newCameraPos;

		Vector3 newAIPos = new Vector3 (AIFollower.transform.position.x - distanceToFirstSpawnPosition, AIFollower.transform.position.y, AIFollower.transform.position.z);
		AIFollower.transform.position = newAIPos;
	}

	public void StartGame(){
		hasBegin = true;
	}

}
